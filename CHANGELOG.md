## 0.5.0

- Renamed `broadcastToSyncStates` to `broadcastToListeners`
- We can now add any Function(dynamic) to a broadcast (as long as we have a key)
- Added tests

## 0.4.3

- Added the ability for `SyncState2`s to subscribe to broadcasts.
- Added additional helper functions.

## 0.4.2

- Added the `onUpdate` callback to `sync_notifier`

## 0.4.1

- Fixed a bug that happens in tests due to widgets being disposed between a notification being scheduled and it being received

## 0.4.0

- **BREAKING** `forEachState<T>()` now by default schedules the callback to execute in a scheduled frame to avoid calling `setState()` during build
- Added the ability to get a value from `SyncState2<T>` using `getStateValue<T, R>()`
- Added `SyncValueNotifier` which is like `ValueNotifier` but simpler to use

## 0.3.2

- Added `StateGroup2` and `SyncStateBuilder2` which work similarly to the version 1 variants but with `SyncState2`
- License is now BSD 3-Clause

## 0.3.1

- Added `SyncState2<T>` which is now the preferred method (legacy method will still work).
- Updated readme and added associated tests

## 0.3.0

- Added the `onlyOneAllowed` and `queueMessages` parameters to `StateGroup<T>`
- **BREAKING** removed `errorFunction` in `SyncState<T>`
- Updated readme with `SyncStateBuilder<T>`

## 0.2.1

- Added `SyncStateBuilder` which allows you to use `state_groups` without making a custom widget
- Upgraded to Dart 2.17

## 0.2.0

- Updated to Flutter 3
- Removed the `!` operator in `WidgetsBinding.instance!` because it is no necessary in Flutter 3

## 0.1.5

- Have to call `WidgetsBinding.instance!.scheduleFrame()` after `WidgetsBinding.instance!.addPostFrameCallback()` otherwise there's no guarantee the scheduled command is actually run

## 0.1.4

- `onNotify()` should have a return type of void not null

## 0.1.3

- Now you can make state groups run custom logic when activated

## 0.1.2

- Updates to documentation and added example

## 0.1.1

- The argument to `notifyAll()` should actually be optional

## 0.1.0

- **BREAKING** You can now pass custom message types to state groups (not just int)

## 0.0.1

- Initial release
