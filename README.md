# state_groups

## Stateless state management solution

<!-- Alternate subtitle: State management without the baggage of having to manage state -->

Most state management solutions are overly convoluted because they try to do two things: manage the internal state and update the visible state. State groups, on the other hand, only updates the visible state. You have to manage the internal state yourself. This way it can be significantly simpler than other state management solutions.

# Usage

Version 0.3.1 significantly simplifies the process. The following method is preferred although you can still use the legacy method (described at the end).

This package works by notifying `State` in a `StatefulWidget`. To do so simply convert the `State` that your State inherits from to a `SyncState`. So

```dart
class FooState extends State<Foo>
```

becomes

```dart
class FooState extends SyncState<void, Foo>
```

## Notifying

To notify all the `SyncState`s of a given type simply call `updateAll<T>()` where `T` is the type you want to notify. IE:

```dart
updateAll<FooState>();
```

If you want to send more complicated messages call `forEachState<T>(void Function(T))` again with `T` being the type you want to notify. IE:

```dart
forEachState<FooState>((FooState state) {
	state.someInternalMethod();
});
```

# Usage (Legacy)

## Creating State Groups

First create a state group. For this example we will create a global variable although they don't have to be global.

```dart
StateGroup exampleStateGroup = StateGroup();
```

You can also specify a generic type as the message type. Here we'll use `void` as we won't be sending any messages but if you wanted to send a message you could use an enum instead.

```dart
StateGroup<void> exampleStateGroup = StateGroup<void>();
```

## Registering To A StateGroup

There are two steps here. First change the state class to extend from `SyncState` instead of `State`. This will also requiring adding an additional generic type. This should be the same generic type used in the previous step We'll leave this as `void` for now.

```dart
class FooState extends State<Foo>
```

becomes

```dart
class FooState extends SyncState<void, Foo>
```

Then make the constructor call super with the state group you would like to subscribe to.

```dart
FooState() : super(exampleStateGroup);
```

## Notifying A StateGroup

Now you can notify a `StateGroup` whenever you want all the members of the group to update or check if they should update. To do so use the `notifyAll()` command like so:

```dart
exampleStateGroup.notifyAll();
```

And that's it. The `StateGroup` will handle everything else automatically.

## SyncStateBuilder<T>

If this is too much work for you there is also `SyncStateBuilder<T>` which works as a `SyncState<void>`. It should be enough if all you want is to have the UI update on a notification but it does not support custom update behaviour.
