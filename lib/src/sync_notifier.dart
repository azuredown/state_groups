
import '../state_groups.dart';

class SyncValueNotifier<T> {

	StateGroup2 _statesToNotify = StateGroup2();

	SyncValueNotifier({this.value, this.onUpdate});

	T? value;
	Function({T? newValue, T? oldValue})? onUpdate;

	void setValue(T? newValue) {
		if (onUpdate != null) {
			onUpdate!(
				oldValue: value,
				newValue: newValue,
			);
		}
		value = newValue;
		_statesToNotify.updateAll();
	}

	T? getValueAndUpdates(SyncState2 subscriber) {
		subscriber.addAndLinkGroup(_statesToNotify);
		return value;
	}

}