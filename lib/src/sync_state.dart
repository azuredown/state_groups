
import 'package:flutter/widgets.dart';

Map<Type, Set<SyncState2<dynamic>>> _syncStates = <Type, Set<SyncState2<dynamic>>>{};
Map<Type, List<void Function()>> _syncStateQueues = <Type, List<void Function()>>{};

int getNumberOfSyncStates() => _syncStates.length;
int getNumberOfSyncStateQueues() => _syncStateQueues.length;

Map<String, Map<Object, Function(dynamic)>> broadcastListeners = <String, Map<Object, Function(dynamic)>>{};

void broadcastToListeners(dynamic key, dynamic message) {
	Map<Object, Function(dynamic)>? listeners = broadcastListeners[key];
	if (listeners != null) {
		for (final Function(dynamic) callback in listeners.values) {
			callback(message);
		}
	}
}

void addBroadcastListener(String message, Object key, Function(dynamic data) listener) {

	Map<Object, dynamic Function(dynamic)>? listeners = broadcastListeners[message];

	if (listeners == null) {
		broadcastListeners[message] = { key : listener };
	} else {

		assert(!listeners.containsKey(key), "$key added to $message multiple times");

		listeners[key] = listener;
	}
}

void removeBroadcastListener(String message, Object key) {
	Map<Object, dynamic Function(dynamic)>? broadcasts = broadcastListeners[message];

	if (broadcasts == null) {
		return;
	}

	broadcasts.remove(key);

	if (broadcasts.length == 0) {
		broadcastListeners.remove(message);
	}
}

ReturnType? tryGetStateValue<T extends SyncState2<dynamic>, ReturnType>(ReturnType Function(T state) function) {
	final Set<SyncState2<dynamic>> items = _syncStates[T]!;
	assert(items.length <= 1);
	if (items.isEmpty) {
		return null;
	} else {
		return function(items.first as T);
	}
}

ReturnType getStateValue<T extends SyncState2<dynamic>, ReturnType>(ReturnType Function(T state) function) {
	assert(_syncStates[T]!.length == 1);
	return function(_syncStates[T]!.first as T);
}

ReturnType getAggregatedStateValue<T extends SyncState2<dynamic>, ReturnType>(ReturnType Function(Set<T> state) function) {
	return function(Set<T>.from(_syncStates[T]!));
}

ReturnType? getFirstStateValueWhere<T extends SyncState2<dynamic>, ReturnType>(bool shouldGetValue(T state), ReturnType Function(T state) getValue) {
	for (SyncState2<dynamic> state in _syncStates[T]!) {
		if (shouldGetValue(state as T)) {
			return getValue(state);
		}
	}
	return null;
}

/// Updates the UI (setState()) on all [SyncState2]s of the given type
void updateAll<T extends SyncState2<dynamic>>() {
	forEachState<T>((T item) => item.update(useCallback: false));
}

void updateWhere<T extends SyncState2<dynamic>>(bool Function(T) condition) {
	forEachState<T>((T item) {
		if (condition(item)) {
			item.update(useCallback: false);
		}
	});
}

/// Calls each [SyncState2] of the given type and does the given action on it.
/// If the [maxQueueSize] is given messages will be queued if they fail there are no listeners.
/// and throw an assertion error if the max queue size is exceeded.
/// There must be at least one instance of the widget currently active.
/// Note that if [useCallback] is true it is possible that the widget is
/// destroyed between us sending the message and the widget having received it.
/// For this reason it is recommended that functions passed only modify the widget being called.
void forEachState<T extends SyncState2<dynamic>>(Function(T state) function, {int? maxQueueSize, bool useCallback = true}) {
	assert(maxQueueSize == null || maxQueueSize > 0);
	if (!_syncStates.containsKey(T)) {
		if (maxQueueSize != null) {
			if (!_syncStateQueues.containsKey(T)) {
				_syncStateQueues[T] = <void Function()>[ () {
					forEachState<T>(function, useCallback: false);
				}];
			} else {
				assert(_syncStateQueues[T]!.length < maxQueueSize);
				_syncStateQueues[T]!.add(() {
					forEachState<T>(function, useCallback: false);
				});
			}
		}
		return;
	}
	if (!useCallback) {
		for (final SyncState2<dynamic> state in _syncStates[T]!) {
			function(state as T);
		}
	} else {
		assert(_syncStates.containsKey(T));
		WidgetsBinding.instance.addPostFrameCallback((_) {

			// It is possible for the key to have been removed between scheduling
			// However this appears to only happen in tests
			if (!_syncStates.containsKey(T)) {
				return;
			}

			for (final SyncState2<dynamic> state in _syncStates[T]!) {
				function(state as T);
			}
		});
		WidgetsBinding.instance.scheduleFrame();
	}
}

/// A version of [SyncState] that does not rely on StateGroups.
/// In order to use it use the [updateAll] and [forEachState] functions above
abstract class SyncState2<T extends StatefulWidget> extends State<T> {

	SyncState2({StateGroup2? group}) {
		if (group != null) {
			addAndLinkGroup(group);
		}
	}

	final Set<StateGroup2> groupsSubscribedTo = <StateGroup2>{};
	final Set<String> openBroadcasts = <String>{};

	void addAndLinkGroup(StateGroup2 group) {
		group.addSyncState(this);
		groupsSubscribedTo.add(group);
	}

	void subscribeToBroadcasts(String message, {Function(dynamic data)? listener, final bool useCallback = true}) {
		assert(openBroadcasts.contains(message) == false);
		openBroadcasts.add(message);

		final Function(dynamic data) callback;
		if (listener == null) {
			callback = (_) => update();
		} else if (!useCallback) {
			callback = listener;
		} else {
			callback = (dynamic data) {
				WidgetsBinding.instance.addPostFrameCallback((_) {
					listener(data);
				});
				WidgetsBinding.instance.scheduleFrame();
			};
		}

		addBroadcastListener(message, this, callback);
	}

	@override
	void initState() {
		super.initState();
		if (!_syncStates.containsKey(runtimeType)) {
			_syncStates[runtimeType] = <SyncState2<dynamic>>{ this };
		} else {
			_syncStates[runtimeType]!.add(this);
		}
		if (_syncStateQueues.containsKey(runtimeType)) {
			for (final void Function() value in _syncStateQueues[runtimeType]!) {
				value();
			}
			_syncStateQueues.remove(runtimeType);
		}
	}

	@override
	void dispose() {

		for (StateGroup2 group in groupsSubscribedTo) {
			group.removeSyncState(this);
		}

		for (final String message in openBroadcasts) {
			removeBroadcastListener(message, this);
		}

		_syncStates[runtimeType]!.remove(this);
		if (_syncStates[runtimeType]!.isEmpty) {
			_syncStates.remove(runtimeType);
		}
		super.dispose();
	}

	/// Use this to call setState(). Calling setState() directly is not safe.
	/// It is not recommended to override this, unlike the original [SyncState]
	void update({final bool useCallback = true}) {
		if (!useCallback) {
			if (mounted) {
				setState(() {});
			}
			return;
		}
		WidgetsBinding.instance.addPostFrameCallback((_) {
			if (mounted) {
				setState(() {});
			}
		});
		WidgetsBinding.instance.scheduleFrame();
	}
}

/// Make your state inherit from [SyncState] instead of [State]. That will allow
/// this class to do all the message handling stuff for you. Note that update
/// and shouldUpdate() can be overridden for custom behaviour.
abstract class SyncState<MessageType, T extends StatefulWidget> extends State<T> {

	// This causes a runtime error if a stategroup with the wrong generic type
	// is given, not a compile time error ¯\_(ツ)_/¯
	SyncState(StateGroup<MessageType> group) {
		_groups.add(group);
	}

	/// This is only for legacy reasons. Ignore it.
	SyncState.Advanced(this._groups);

	List<StateGroup<MessageType>> _groups = <StateGroup<MessageType>>[];

	@override
	void initState() {
		for (int i = 0; i < _groups.length; i++) {
			_groups[i].addSyncState(this);
		}
		super.initState();
	}

	@override
	void dispose() {
		for (int i = 0; i < _groups.length; i++) {
			_groups[i].removeSyncState(this);
		}
		super.dispose();
	}

	/// By default this calls setState() if we should update. Override it for
	/// custom behaviour.
	void update(MessageType? message) {
		if (!shouldUpdate(message)) {
			return;
		}
		WidgetsBinding.instance.addPostFrameCallback((_) {
			if (mounted) {
				setState(() {});
			}
		});
		WidgetsBinding.instance.scheduleFrame();
	}

	/// This is checked by update() and not notifyAll() in the [StateGroup] I may
	/// change it in the future
	bool shouldUpdate(MessageType? message) {
		return true;
	}
}

/// A collection of [SyncState2] objects. Use updateAll() to notify them all.
class StateGroup2 {

	final Set<SyncState2<dynamic>> _states = <SyncState2<dynamic>>{};

	int numStates() => _states.length;

	void addSyncState(SyncState2<dynamic> state) {
		_states.add(state);
	}

	void removeSyncState(SyncState2<dynamic> state) {
		assert(_states.contains(state));
		_states.remove(state);
	}

	/// Updates all states
	void updateAll() {
		for (final SyncState2<dynamic> state in _states) {
			state.update();
		}
	}
}

/// A collection of [SyncState] objects. Use notifyAll() to notify them all.
class StateGroup<MessageType> {

	StateGroup({
		this.onNotify,
		this.onlyOneAllowed = false,
		bool queueMessages = false,
	}) {
		if (queueMessages) {
			messageQueue = <MessageType>[];
		}
	}

	/// Block duplicate sync states.
	final bool onlyOneAllowed;

	/// Called before notifying.
	final void Function(MessageType?)? onNotify;

	List<MessageType?>? messageQueue;
	final Set<SyncState<MessageType, dynamic>> _states = <SyncState<MessageType, dynamic>>{};

	void addSyncState(SyncState<MessageType, dynamic> state) {
		if (onlyOneAllowed) {
			assert(_states.isEmpty);
		}
		_states.add(state);
		if (messageQueue != null) {
			for (int i = 0; i < messageQueue!.length; i++) {
				notifyAll(messageQueue![i]);
			}
			messageQueue!.clear();
		}
	}

	void removeSyncState(SyncState<MessageType, dynamic> state) {
		assert(_states.contains(state));
		_states.remove(state);
	}

	/// Updates all states with the given message
	void notifyAll([MessageType? message]) {
		if (onNotify != null) {
			onNotify!(message);
		}
		if (_states.isEmpty && messageQueue != null) {
			messageQueue!.add(message);
		}
		for (final SyncState<MessageType, dynamic> state in _states) {
			state.update(message);
		}
	}

	@override
	String toString() {
		return "State Group ($_states)";
	}
}

/// A collection of [StateGroup] objects. Use notifyAll() to notify them all.
class StateGroupCollection<MessageType> {

	StateGroupCollection(this.collections);

	final List<StateGroup<MessageType>> collections;

	/// Updates all states with the given message
	void notifyAll([MessageType? message]) {
		for (int i = 0; i < collections.length; i++) {
			collections[i].notifyAll(message);
		}
	}

	@override
	String toString() {
		return "State Group Collection: ($collections)";
	}
}

class SyncStateBuilder<T> extends StatefulWidget {
	const SyncStateBuilder({required this.stateGroup, required this.builder, super.key});

	final StateGroup<T> stateGroup;
	final Widget Function(BuildContext context) builder;

	@override
	State<SyncStateBuilder<T>> createState() => _SyncStateBuilderState<T>(stateGroup);
}

class _SyncStateBuilderState<T> extends SyncState<T, SyncStateBuilder<T>> {

	_SyncStateBuilderState(super.stateGroup);

	@override
	Widget build(BuildContext context) {
		return widget.builder(context);
	}
}

class SyncStateBuilder2 extends StatefulWidget {
	const SyncStateBuilder2({required this.stateGroup, required this.builder, super.key});

	final StateGroup2 stateGroup;
	final Widget Function(BuildContext context) builder;

	@override
	State<SyncStateBuilder2> createState() => _SyncStateBuilderState2(group: stateGroup);
}

class _SyncStateBuilderState2 extends SyncState2<SyncStateBuilder2> {

	_SyncStateBuilderState2({super.group});

	@override
	Widget build(BuildContext context) {
		return widget.builder(context);
	}
}
