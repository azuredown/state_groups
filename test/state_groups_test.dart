
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../lib/state_groups.dart';

class TestSyncStateState extends SyncState<int, StatefulWidget> {

	TestSyncStateState(super.group);

	int counter = 0;

	@override
	void update(int? message) {
		counter += message ?? 0;
	}

	@override
	Widget build(BuildContext context) {
		return const SizedBox();
	}
}

void main() {

	final StateGroup<void> stateGroup = StateGroup<void>();

	testWidgets('SyncStateWrapper<T> updates data correctly', (WidgetTester tester) async {
		int counter = 1;
		expect(counter, 1);
		await tester.pumpWidget(
			MaterialApp(
				home: SyncStateBuilder<void>(
					stateGroup: stateGroup,
					builder: (_) {
						counter++;
						return Text(counter.toString());
					},
				),
			),
		);
		expect(counter, 2);
		expect(find.text("2"), findsOneWidget);
		stateGroup.notifyAll();
		tester.binding.scheduleWarmUpFrame();
		await tester.pumpAndSettle();
		expect(counter, 3);
		expect(find.text("3"), findsOneWidget);
	});

	test('Queue works correctly', () {

		StateGroup<int> stateGroup = StateGroup<int>(queueMessages: true);

		stateGroup.notifyAll(100);
		stateGroup.notifyAll(10);
		stateGroup.notifyAll(20);

		TestSyncStateState testSyncStateState = TestSyncStateState(stateGroup);
		testSyncStateState.initState();
		expect(testSyncStateState.counter, 130);

		stateGroup = StateGroup<int>();

		stateGroup.notifyAll(100);
		stateGroup.notifyAll(10);
		stateGroup.notifyAll(20);

		testSyncStateState = TestSyncStateState(stateGroup);
		testSyncStateState.initState();
		expect(testSyncStateState.counter, 0);

	});

	test('Only one allowed', () {

		StateGroup<int> stateGroup = StateGroup<int>(onlyOneAllowed: true);

		TestSyncStateState(stateGroup).initState();
		expect(() { TestSyncStateState(stateGroup).initState(); }, throwsAssertionError);

		stateGroup = StateGroup<int>();

		TestSyncStateState(stateGroup).initState();
		TestSyncStateState(stateGroup).initState();
		TestSyncStateState(stateGroup).initState();

	});
}
