
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:state_groups/src/sync_state.dart';

class TestWidget extends StatefulWidget {
	const TestWidget({required this.name, this.stateGroup, super.key});

	final StateGroup2? stateGroup;
	final String name;

	@override
	State<TestWidget> createState() => TestWidgetState(group: stateGroup);
}

class TestWidgetState extends SyncState2<TestWidget> {

	TestWidgetState({super.group});

	int timesRedrawn = 0;
	int counter = 0;

	void incrementCount(int amount) {
		counter += amount;
	}

	@override
	Widget build(BuildContext context) {
		timesRedrawn++;
		return Text("${widget.name} drawn: $timesRedrawn times, counter: $counter");
	}
}

class TestWidget2 extends StatefulWidget {
	const TestWidget2({required this.name, this.stateGroup, super.key});

	final StateGroup2? stateGroup;
	final String name;

	@override
	State<TestWidget2> createState() => TestWidgetState2(group: stateGroup);
}

class TestWidgetState2 extends SyncState2<TestWidget2> {

	TestWidgetState2({super.group});

	int timesRedrawn = 0;
	int counter = 0;

	void incrementCount(int amount) {
		counter += amount;
	}

	@override
	Widget build(BuildContext context) {
		timesRedrawn++;
		return Text("${widget.name} drawn: $timesRedrawn times, counter: $counter");
	}
}

class TestWidgetGeneric<T> extends StatefulWidget {
	const TestWidgetGeneric({required this.name, super.key});

	final String name;

	@override
	State<TestWidgetGeneric<T>> createState() => _TestWidgetGenericState<T>();
}

class _TestWidgetGenericState<T> extends SyncState2<TestWidgetGeneric<T>> {

	int timesRedrawn = 0;
	int counter = 0;

	void incrementCount(int amount) {
		counter += amount;
	}

	@override
	Widget build(BuildContext context) {
		timesRedrawn++;
		return Text("${widget.name} drawn: $timesRedrawn times, counter: $counter");
	}
}

void main() {

	Future<void> buildAndGetWidget(WidgetTester tester, int expectedTimesRedrawn, int expectedCounter) async {
		await tester.pumpWidget(
			const MaterialApp(
				home: TestWidget(name: "a"),
			),
		);
		expect(find.text("a drawn: $expectedTimesRedrawn times, counter: $expectedCounter"), findsOneWidget, reason: find.textContaining("").toString());
	}

	Future<void> buildAndGet2Widgets(WidgetTester tester) async {
		await tester.pumpWidget(
			const MaterialApp(
				home: Column(
					children: <Widget>[
						TestWidget(name: "a"),
						TestWidget2(name: "b"),
					],
				),
			),
		);
	}

	final StateGroup2 testStateGroup = StateGroup2();

	Future<void> buildAndGetMultipleWidgets(WidgetTester tester) async {
		await tester.pumpWidget(
			MaterialApp(
				home: Column(
					children: <Widget>[
						const TestWidget(name: "a"),
						TestWidget2(name: "b", stateGroup: testStateGroup),
						SyncStateBuilder2(
							stateGroup: testStateGroup,
								// ignore: prefer_const_constructors, if we use const it will not be rebuilt and tests will fail
							builder: (_) => TestWidget(name: "c", key: Key("1")),
						),
						SyncStateBuilder2(
							stateGroup: testStateGroup,
							// ignore: prefer_const_constructors, if we use const it will not be rebuilt and tests will fail
							builder: (_) => TestWidget2(name: "d", key: Key("2")),
						),
					],
				),
			),
		);
	}

	testWidgets('Updates', (WidgetTester tester) async {
		updateAll<TestWidgetState>();
		await buildAndGetWidget(tester, 1, 0);
		updateAll<TestWidgetState>();
		await tester.pumpAndSettle();
		expect(find.textContaining("a drawn: 2 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
	});

	testWidgets('Generics', (WidgetTester tester) async {
		updateAll<_TestWidgetGenericState<dynamic>>();
		await tester.pumpWidget(
			const MaterialApp(
				home: TestWidgetGeneric<int>(name: "a"),
			),
		);
		expect(find.text("a drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		updateAll<_TestWidgetGenericState<dynamic>>();
		await tester.pumpAndSettle();

		expect(find.text("a drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		updateAll<_TestWidgetGenericState<int>>();
		await tester.pumpAndSettle();
		expect(find.textContaining("a drawn: 2 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
	});

	testWidgets('Only Update Desired Objects', (WidgetTester tester) async {
		updateAll<TestWidgetState>();
		await buildAndGet2Widgets(tester);
		expect(find.textContaining("a drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("b drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		updateAll<TestWidgetState2>();
		await tester.pumpAndSettle();
		expect(find.textContaining("a drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("b drawn: 2 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
	});

	testWidgets('Only Update Desired Objects With Builder', (WidgetTester tester) async {
		updateAll<TestWidgetState>();
		await buildAndGetMultipleWidgets(tester);
		expect(find.textContaining("a drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("b drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("c drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("d drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		updateAll<TestWidgetState2>();
		await tester.pumpAndSettle();
		expect(find.textContaining("a drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("b drawn: 2 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("c drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("d drawn: 2 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		testStateGroup.updateAll();
		await tester.pumpAndSettle();
		expect(find.textContaining("a drawn: 1 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("b drawn: 3 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("c drawn: 2 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
		expect(find.textContaining("d drawn: 3 times, counter: 0"), findsOneWidget, reason: find.textContaining("").toString());
	});

	testWidgets('Cleanup is done properly (SyncState2)', (WidgetTester tester) async {
		expect(getNumberOfSyncStates(), 0);
		await buildAndGetWidget(tester, 1, 0);
		expect(getNumberOfSyncStates(), 1);
		await tester.pumpWidget(
			const MaterialApp(
				home: SizedBox(),
			),
		);
		expect(getNumberOfSyncStates(), 0);
	});

	testWidgets('Cleanup is done properly (SyncStateBuilder2)', (WidgetTester tester) async {
		expect(getNumberOfSyncStates(), 0);
		expect(testStateGroup.numStates(), 0);
		await tester.pumpWidget(
			MaterialApp(
				home: SyncStateBuilder2(
					stateGroup: testStateGroup,
					builder: (_) => const SizedBox.shrink(),
				),
			),
		);
		expect(testStateGroup.numStates(), 1);
		expect(getNumberOfSyncStates(), 1);
		await tester.pumpWidget(
			const MaterialApp(
				home: SizedBox(),
			),
		);
		expect(getNumberOfSyncStates(), 0);
		expect(testStateGroup.numStates(), 0);
	});

	testWidgets('Increment Count', (WidgetTester tester) async {
		forEachState<TestWidgetState>((TestWidgetState state) {
			state.incrementCount(1);
		});
		await buildAndGetWidget(tester, 1, 0);
		forEachState<TestWidgetState>((TestWidgetState state) {
			state.incrementCount(1);
		});
		updateAll<TestWidgetState>();
		await tester.pumpAndSettle();
		expect(find.textContaining("a drawn: 2 times, counter: 1"), findsOneWidget, reason: find.textContaining("").toString());
	});

	group("Queue", () {

		testWidgets('Cleanup is done correctly (queue)', (WidgetTester tester) async {
			expect(getNumberOfSyncStateQueues(), 0);
			forEachState<TestWidgetState>((TestWidgetState state) {
				state.incrementCount(10);
			}, maxQueueSize: 5);
			expect(getNumberOfSyncStateQueues(), 1);
			forEachState<TestWidgetState>((TestWidgetState state) {
				state.incrementCount(20);
			}, maxQueueSize: 5);
			expect(getNumberOfSyncStateQueues(), 1);
			await buildAndGetWidget(tester, 1, 30);
			expect(getNumberOfSyncStateQueues(), 0);
		});

		testWidgets('Queue, extra space', (WidgetTester tester) async {
			forEachState<TestWidgetState>((TestWidgetState state) {
				state.incrementCount(10);
			}, maxQueueSize: 5, useCallback: false);
			forEachState<TestWidgetState>((TestWidgetState state) {
				state.incrementCount(20);
			}, maxQueueSize: 5, useCallback: false);
			await buildAndGetWidget(tester, 1, 30);
		});

		testWidgets('Insufficient space 1', (WidgetTester tester) async {
			forEachState<TestWidgetState>((TestWidgetState state) {
				state.incrementCount(10);
			}, maxQueueSize: 1);
			expect(() {
				forEachState<TestWidgetState>((TestWidgetState state) {
					state.incrementCount(20);
				}, maxQueueSize: 1);
			}, throwsAssertionError);
			await buildAndGetWidget(tester, 1, 10);
		});

		testWidgets('Insufficient space 0', (WidgetTester tester) async {
			expect(() {
				forEachState<TestWidgetState>((TestWidgetState state) {
					state.incrementCount(20);
				}, maxQueueSize: 0);
			}, throwsAssertionError);
			expect(() {
				forEachState<TestWidgetState>((TestWidgetState state) {
					state.incrementCount(20);
				}, maxQueueSize: 0);
			}, throwsAssertionError);
			await buildAndGetWidget(tester, 1, 0);
		});

		testWidgets('Insufficient space -1', (WidgetTester tester) async {
			expect(() {
				forEachState<TestWidgetState>((TestWidgetState state) {
					state.incrementCount(20);
				}, maxQueueSize: -1);
			}, throwsAssertionError);
			expect(() {
				forEachState<TestWidgetState>((TestWidgetState state) {
					state.incrementCount(20);
				}, maxQueueSize: -1);
			}, throwsAssertionError);
			await buildAndGetWidget(tester, 1, 0);
		});

		testWidgets('Queue, exact space', (WidgetTester tester) async {
			forEachState<TestWidgetState>((TestWidgetState state) {
				state.incrementCount(10);
			}, maxQueueSize: 2);
			forEachState<TestWidgetState>((TestWidgetState state) {
				state.incrementCount(20);
			}, maxQueueSize: 2);
			await buildAndGetWidget(tester, 1, 30);
		});

		testWidgets('No queue, no object', (WidgetTester tester) async {
			forEachState<TestWidgetState>((TestWidgetState state) {
				state.incrementCount(10);
			});
			forEachState<TestWidgetState>((TestWidgetState state) {
				state.incrementCount(20);
			});
			await buildAndGetWidget(tester, 1, 0);
		});

	});
}
